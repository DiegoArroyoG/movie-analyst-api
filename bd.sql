CREATE DATABASE IF NOT EXISTS movie_db;
USE movie_db;
DROP TABLE IF EXISTS movies;
DROP TABLE IF EXISTS reviewers;
DROP TABLE IF EXISTS publications;
CREATE TABLE IF NOT EXISTS publications (
   name VARCHAR(250),
   avatar VARCHAR(250),
   PRIMARY KEY (name)
);
CREATE TABLE IF NOT EXISTS reviewers (
   name VARCHAR(250),
   publication VARCHAR(250),
   avatar VARCHAR(250),
   PRIMARY KEY (name),
   FOREIGN KEY (publication) REFERENCES publications(name)
);
CREATE TABLE IF NOT EXISTS movies (
   title VARCHAR(250),
   release_year VARCHAR(250),
   score INT(11),
   reviewer VARCHAR(250),
   publication VARCHAR(250),
   PRIMARY KEY (title),
   FOREIGN KEY (reviewer) REFERENCES reviewers(name)
);